import { swapPositionsValues } from "./helpers.js";

export function quickSort(arrayToSort) {
  let startIndex = 0,
    endIndex = arrayToSort.length - 1;
  quickSortRecursive(arrayToSort, startIndex, endIndex);
  return arrayToSort;
}

function quickSortRecursive(arrayToSort, startIndex, endIndex) {
  if (startIndex < endIndex) {
    let partitioningIndex = partition(arrayToSort, startIndex, endIndex);
    quickSortRecursive(arrayToSort, startIndex, partitioningIndex - 1);
    quickSortRecursive(arrayToSort, partitioningIndex + 1, endIndex);
  }
}

function partition(arrayToSort, startIndex, endIndex) {
  let pivot = arrayToSort[endIndex];
  let i = startIndex - 1;

  for (let j = startIndex; j <= endIndex - 1; j++) {
    if (arrayToSort[j] < pivot) {
      i++;
      swapPositionsValues(arrayToSort, i, j);
    }
  }
  swapPositionsValues(arrayToSort, i + 1, endIndex);
  return i + 1;
}
