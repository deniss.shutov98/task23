export function swapPositionsValues(array, firstPosition, secondPosition) {
  let temp = array[firstPosition];
  array[firstPosition] = array[secondPosition];
  array[secondPosition] = temp;
}
