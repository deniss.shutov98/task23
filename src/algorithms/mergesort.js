export function mergeSort(arrayToSort) {
  let startIndex = 0,
    endIndex = arrayToSort.length - 1;
  mergeSortRecursive(arrayToSort, startIndex, endIndex);
  return arrayToSort;
}

function mergeSortRecursive(arrayToSort, startIndex, endIndex) {
  if (startIndex >= endIndex) {
    return;
  }

  let midIndex = startIndex + parseInt((endIndex - startIndex) / 2);
  mergeSortRecursive(arrayToSort, startIndex, midIndex);
  mergeSortRecursive(arrayToSort, midIndex + 1, endIndex);
  merge(arrayToSort, startIndex, midIndex, endIndex);
}

function merge(arrayToSort, startIndex, midIndex, endIndex) {
  let firstSubarray = arrayToSort.slice(startIndex, midIndex + 1);
  let secondSubarray = arrayToSort.slice(midIndex + 1, endIndex + 1);

  let firstSubarrayInitialIndex = 0;
  let secondSubarrayInitialIndex = 0;
  let mergedSubarrayInitialIndex = startIndex;

  while (
    firstSubarrayInitialIndex < firstSubarray.length &&
    secondSubarrayInitialIndex < secondSubarray.length
  ) {
    if (
      firstSubarray[firstSubarrayInitialIndex] <=
      secondSubarray[secondSubarrayInitialIndex]
    ) {
      arrayToSort[mergedSubarrayInitialIndex] =
        firstSubarray[firstSubarrayInitialIndex];
      firstSubarrayInitialIndex++;
    } else {
      arrayToSort[mergedSubarrayInitialIndex] =
        secondSubarray[secondSubarrayInitialIndex];
      secondSubarrayInitialIndex++;
    }
    mergedSubarrayInitialIndex++;
  }

  mergedSubarrayInitialIndex = copyElements(
    firstSubarrayInitialIndex,
    firstSubarray,
    mergedSubarrayInitialIndex,
    arrayToSort
  );
  mergedSubarrayInitialIndex = copyElements(
    secondSubarrayInitialIndex,
    secondSubarray,
    mergedSubarrayInitialIndex,
    arrayToSort
  );
}

function copyElements(
  subarrayInitialIndex,
  subarray,
  mergedSubarrayInitialIndex,
  arrayToSort
) {
  while (subarrayInitialIndex < subarray.length) {
    arrayToSort[mergedSubarrayInitialIndex] = subarray[subarrayInitialIndex];
    subarrayInitialIndex++;
    mergedSubarrayInitialIndex++;
  }
  return mergedSubarrayInitialIndex;
}
