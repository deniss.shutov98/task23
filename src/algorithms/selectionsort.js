import { swapPositionsValues } from "./helpers.js";

export function selectionSort(arrayToSort) {
  let j, minimumElementIndex;
  for (let i = 0; i < arrayToSort.length - 1; i++) {
    minimumElementIndex = i;
    for (j = i + 1; j < arrayToSort.length; j++) {
      if (arrayToSort[j] < arrayToSort[minimumElementIndex]) {
        minimumElementIndex = j;
      }
    }
    swapPositionsValues(arrayToSort, minimumElementIndex, i);
  }
  return arrayToSort;
}
