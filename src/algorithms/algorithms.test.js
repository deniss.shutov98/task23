import { swapPositionsValues } from "./helpers.js";
import { bubbleSort } from "./bubblesort.js";
import { selectionSort } from "./selectionsort.js";
import { insertionSort } from "./insertionsort";
import { mergeSort } from "./mergesort.js";
import { quickSort } from "./quicksort.js";

test("swapPositionValuesTest", () => {
  let testArray = [0, 1];
  swapPositionsValues(testArray, 0, 1);
  expect(testArray.length).toBe(2);
  expect(testArray).toEqual([1, 0]);
});

const sortingTestArray = [4, 9, 1, 0, 4, 8, 5, 10];

test("bubblesortTest", () => {
  let bubblesortArray = bubbleSort(Array.from(sortingTestArray));
  expect(bubblesortArray.length).toBe(8);
  expect(bubblesortArray).toEqual([0, 1, 4, 4, 5, 8, 9, 10]);
});

test("selectionsortTest", () => {
  let selectionsortArray = selectionSort(Array.from(sortingTestArray));
  expect(selectionsortArray.length).toBe(8);
  expect(selectionsortArray).toEqual([0, 1, 4, 4, 5, 8, 9, 10]);
});

test("insertionsortTest", () => {
  let insertionsortArray = insertionSort(Array.from(sortingTestArray));
  expect(insertionsortArray.length).toBe(8);
  expect(insertionsortArray).toEqual([0, 1, 4, 4, 5, 8, 9, 10]);
});

test("mergesortTest", () => {
  let mergesortArray = mergeSort(Array.from(sortingTestArray));
  expect(mergesortArray.length).toBe(8);
  expect(mergesortArray).toEqual([0, 1, 4, 4, 5, 8, 9, 10]);
});

test("quicksortTest", () => {
  let quicksortArray = quickSort(Array.from(sortingTestArray));
  expect(quicksortArray.length).toBe(8);
  expect(quicksortArray).toEqual([0, 1, 4, 4, 5, 8, 9, 10]);
});
