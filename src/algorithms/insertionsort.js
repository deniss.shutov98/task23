export function insertionSort(arrayToSort) {
  for (let i = 1; i < arrayToSort.length; i++) {
    let key = arrayToSort[i];
    let j = i - 1;
    while (j >= 0 && arrayToSort[j] > key) {
      arrayToSort[j + 1] = arrayToSort[j];
      j = j - 1;
    }
    arrayToSort[j + 1] = key;
  }
  return arrayToSort;
}
