import { swapPositionsValues } from "./helpers.js";

export function bubbleSort(arrayToSort) {
  for (let i = 0; i < arrayToSort.length; i++) {
    for (let j = 0; j < arrayToSort.length - (i - 1); j++) {
      if (arrayToSort[j] > arrayToSort[j + 1]) {
        swapPositionsValues(arrayToSort, j, j + 1);
      }
    }
  }
  return arrayToSort;
}
