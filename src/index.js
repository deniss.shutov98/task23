import "./styles/styles.css";
import { bubbleSort } from "./algorithms/bubblesort.js";
import { selectionSort } from "./algorithms/selectionsort.js";
import { insertionSort } from "./algorithms/insertionsort.js";
import { quickSort } from "./algorithms/quicksort.js";
import { mergeSort } from "./algorithms/mergesort.js";

const unsortedArray = document.getElementById("unsorted-array-container");
const bubblesort = document.getElementById("bubblesort-algorithm");
const selectionsort = document.getElementById("selectionsort-algorithm");
const insertionsort = document.getElementById("insertionsort-algorithm");
const quicksort = document.getElementById("quicksort-algorithm");
const mergesort = document.getElementById("mergesort-algorithm");

const generateArrayBtn = document.getElementById("generate-array-btn");
const arrayLength = 100;
const arrayMaxNumber = 99;

var randomArray;

generateArrayBtn.addEventListener("click", function () {
  generateNewArray();
  runSorting();
});

generateNewArray();
runSorting();

function generateNewArray() {
  randomArray = Array.from({ length: arrayLength }, () =>
    Math.floor(Math.random() * arrayMaxNumber)
  );
  updateArrayNumbers(unsortedArray, randomArray);
}

function runSorting() {
  runSort(bubbleSort, bubblesort);
  runSort(selectionSort, selectionsort);
  runSort(insertionSort, insertionsort);
  runSort(quickSort, quicksort);
  runSort(mergeSort, mergesort);
}

function updateArrayNumbers(parentContainer, numbersArray) {
  parentContainer.textContent = "";
  for (let number of numbersArray) {
    let numberContainer = document.createElement("div");
    numberContainer.classList.add("array-member");
    numberContainer.textContent = number;
    parentContainer.appendChild(numberContainer);
  }
}

function runSort(sortingFunction, sortAlgorithmContainer) {
  let sortedArrayContainer =
    sortAlgorithmContainer.getElementsByClassName("array-container")[0];
  let algorithmResults =
    sortAlgorithmContainer.getElementsByClassName("algorithm-results")[0];
  let start = performance.now();
  let sortedArray = sortingFunction(Array.from(randomArray));
  updateArrayNumbers(sortedArrayContainer, sortedArray);
  let duration = performance.now() - start;

  algorithmResults.textContent = `${sortingFunction.name} working time is ${duration} milliseconds`;
}
